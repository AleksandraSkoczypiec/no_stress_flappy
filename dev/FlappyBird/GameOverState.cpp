#include "GameOverState.h"
//#include <sstream>
#include "DEFINITIONS.h"

#include <iostream>
#include <fstream>
namespace FlappyBird
{
	GameOverState::GameOverState(GameDataRef data, int score) : _data(data), _score(score)
	{

	}

	void GameOverState::Init()
	{
		std::ifstream readFile;
		readFile.open("Resources/HighScore.txt");

		if (readFile.is_open())
		{
			while (!readFile.eof())
			{
				readFile >> _highScore;
			}
		}

		readFile.close();

		std::ofstream writeFile("Resources/HighScore.txt");

		if (writeFile.is_open())
		{
			if (_score > _highScore)
			{
				_highScore = _score;
			}

			writeFile << _highScore;
		}

		writeFile.close();
		//std::cout << "Game State" << std::endl;

		this->_data->assets.LoadTexture("Game Over Background", GAME_OVER_BACKGROUND_FILEPATH);
		this->_data->assets.LoadTexture("Game Over Title", GAME_OVER_TITLE_FILEPATH);
		this->_data->assets.LoadTexture("Game Over Body", GAME_OVER_BODY_FILEPATH);
		this->_data->assets.LoadTexture("Bronze Medal", BRONZE_MEDAL_FILEPATH);
		this->_data->assets.LoadTexture("Silver Medal", SILVER_MEDAL_FILEPATH);
		this->_data->assets.LoadTexture("Gold Medal", GOLD_MEDAL_FILEPATH);
		this->_data->assets.LoadTexture("Platinum Medal", PLATINUM_MEDAL_FILEPATH);

		this->_background.setTexture(this->_data->assets.GetTexture("Game Over Background"));
		this->_gameOverTitle.setTexture(this->_data->assets.GetTexture("Game Over Title"));
		this->_gameOverContainer.setTexture(this->_data->assets.GetTexture("Game Over Body"));
		this->_retryButton.setTexture(this->_data->assets.GetTexture("Play Button"));

		this->_gameOverContainer.setPosition(this->_data->window.getSize().x / 2 - this->_gameOverContainer.getGlobalBounds().width / 2, this->_data->window.getSize().y / 2 - this->_gameOverContainer.getGlobalBounds().height / 2);
		this->_gameOverTitle.setPosition(this->_data->window.getSize().x / 2 - this->_gameOverTitle.getGlobalBounds().width / 2, this->_gameOverContainer.getPosition().y - this->_gameOverTitle.getGlobalBounds().height * 1.2);
		this->_retryButton.setPosition(this->_data->window.getSize().x / 2 - this->_retryButton.getGlobalBounds().width / 2, this->_gameOverContainer.getPosition().y + this->_gameOverContainer.getGlobalBounds().height + this->_retryButton.getGlobalBounds().height * 0.2);

		this->_scoreText.setFont(this->_data->assets.GetFont("Flappy Font"));
		this->_scoreText.setString(std::to_string(_score));
		this->_scoreText.setCharacterSize(56);
		this->_scoreText.setFillColor(sf::Color::White);
		this->_scoreText.setOrigin(this->_scoreText.getGlobalBounds().width / 2, this->_scoreText.getGlobalBounds().height / 2);
		this->_scoreText.setPosition(this->_data->window.getSize().x / 10 * 7.25, this->_data->window.getSize().y / 2.15);

		this->_highScoreText.setFont(this->_data->assets.GetFont("Flappy Font"));
		this->_highScoreText.setString(std::to_string(this->_highScore));
		this->_highScoreText.setCharacterSize(56);
		this->_highScoreText.setFillColor(sf::Color::White);
		this->_highScoreText.setOrigin(this->_highScoreText.getGlobalBounds().width / 2, this->_highScoreText.getGlobalBounds().height / 2);
		this->_highScoreText.setPosition(this->_data->window.getSize().x / 10 * 7.25, this->_data->window.getSize().y / 1.78);
	
		if (_score >= PLATINUM_MEDAL_SCORE)
		{
			_medal.setTexture(_data->assets.GetTexture("Platinum Medal"));
		}
		else if (_score >= GOLD_MEDAL_SCORE)
		{
			_medal.setTexture(_data->assets.GetTexture("Gold Medal"));
		}
		else if (_score >= SILVER_MEDAL_SCORE)
		{
			_medal.setTexture(_data->assets.GetTexture("Silver Medal"));
		}
		else
		{
			_medal.setTexture(_data->assets.GetTexture("Bronze Medal"));
		}
		_medal.setPosition(175, 465);
	}

	void GameOverState::HandleInput()
	{
		sf::Event event;

		while (this->_data->window.pollEvent(event))
		{
			if (sf::Event::Closed == event.type)
			{
				this->_data->window.close();
			}

			if (_data->input.IsSpriteClicked(_retryButton, sf::Mouse::Left, _data->window))
			{
				_data->machine.AddState(StateRef(new GameState(_data)), true);
			}
		}
	}

	void GameOverState::Update(float dt)
	{

	}

	void GameOverState::Draw(float dt)
	{
		this->_data->window.clear();

		this->_data->window.draw(this->_background);
		this->_data->window.draw(this->_gameOverContainer);
		this->_data->window.draw(this->_gameOverTitle);
		this->_data->window.draw(this->_retryButton);
		this->_data->window.draw(this->_scoreText);
		this->_data->window.draw(this->_highScoreText);

		this->_data->window.draw(this->_medal);

		this->_data->window.display();
	}
}
