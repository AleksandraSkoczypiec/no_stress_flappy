#pragma once

#define SCREEN_WIDTH 576//768
#define SCREEN_HEIGHT 768//1024

#define SPLASH_STATE_SHOW_TIME 3.0
#define SPLASH_SCENE_BACKGROUND_FILEPATH "Resources/res/flappy icons75/Splash Background.png"//"Resources/res/Splash Background.png"

#define MAIN_MENU_BACKGROUND_FILEPATH "Resources/res/flappy icons75/sky.png"//"Resources/res/sky.png"
#define GAME_TITLE_FILEPATH "Resources/res/flappy icons75/title.png"//"Resources/res/title.png"
#define PLAY_BUTTON_FILEPATH "Resources/res/flappy icons75/PlayButton.png"//"Resources/res/PlayButton.png"

#define GAME_BACKGROUND_FILEPATH "Resources/res/flappy icons75/sky.png"//"Resources/res/sky.png"

#define GAME_OVER_BACKGROUND_FILEPATH "Resources/res/flappy icons75/sky.png"//"Resources/res/sky.png"

#define PIPE_UP_FILEPATH "Resources/res/flappy icons75/PipeUp.png"//"Resources/res/PipeUp.png"
#define PIPE_DOWN_FILEPATH "Resources/res/flappy icons75/PipeDown.png"//"Resources/res/PipeDown.png"
#define SCORING_PIPE_FILEPATH "Resources/res/flappy icons75/InvisibleScoringPipe.png"//"Resources/res/InvisibleScoringPipe.png"

#define LAND_FILEPATH "Resources/res/flappy icons75/Land.png"//"Resources/res/Land.png"

#define BIRD_FRAME_1_FILEPATH "Resources/res/flappy icons75/bird-01.png"//"Resources/res/bird-01.png"
#define BIRD_FRAME_2_FILEPATH "Resources/res/flappy icons75/bird-02.png"//"Resources/res/bird-02.png"
#define BIRD_FRAME_3_FILEPATH "Resources/res/flappy icons75/bird-03.png"//"Resources/res/bird-03.png"
#define BIRD_FRAME_4_FILEPATH "Resources/res/flappy icons75/bird-04.png"//"Resources/res/bird-04.png"

#define FLAPPY_FONT_FILEPATH "Resources/fonts/FlappyFont.ttf"

#define GAME_OVER_TITLE_FILEPATH "Resources/res/flappy icons75/Game-Over-Title.png"//"Resources/res/Game-Over-Title.png"
#define GAME_OVER_BODY_FILEPATH "Resources/res/flappy icons75/Game-Over-Body.png"//"Resources/res/Game-Over-Body.png"

#define BRONZE_MEDAL_FILEPATH "Resources/res/flappy icons75/Bronze-Medal.png"//"Resources/res/Bronze-Medal.png"
#define SILVER_MEDAL_FILEPATH "Resources/res/flappy icons75/Silver-Medal.png"//"Resources/res/Silver-Medal.png"
#define GOLD_MEDAL_FILEPATH "Resources/res/flappy icons75/Gold-Medal.png"//"Resources/res/Gold-Medal.png"
#define PLATINUM_MEDAL_FILEPATH "Resources/res/flappy icons75/Platinum-Medal.png"//"Resources/res/Platinum-Medal.png"

#define PIPE_MOVEMENT_SPEED 200.0f
#define PIPE_SPAWN_FREQUENCY 2.5f

#define BIRD_ANIMATION_DURATION 0.4f

#define BIRD_STATE_STILL 1
#define BIRD_STATE_FALLING 2
#define BIRD_STATE_FLYING 3

#define GRAVITY 350.0f
#define FLYING_SPEED 350.0f

#define FLYING_DURATION 0.25f

#define ROTATION_SPEED 100.0f

enum GameStates
{
	eReady,
	ePlaying,
	eGameOver
};

#define FLASH_SPEED 1500.0f

#define TIME_BEFORE_GAME_OVER_APPEARS 1.5f

#define BRONZE_MEDAL_SCORE 0
#define SILVER_MEDAL_SCORE 5
#define GOLD_MEDAL_SCORE 25
#define PLATINUM_MEDAL_SCORE 100